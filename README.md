# Ionic_docker

[Ionic](https://ionicframework.com/) image to be used with Gitlab CI

## Getting started

### Prerequisites

I assume you have installed Docker and it is running.

See the [Docker website](http://www.docker.io/gettingstarted/#h_installation) for installation instructions :
* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

### Usage

#### Install

Build `docpad/docpad` from source:

    git clone https://gitlab.com/A1CY0N/ionic_docker
    cd ionic_docker
    docker build -t ionic .

#### Run

Run the image, binding associated ports, and mounting the present working
directory:

    docker run -p 8100:8100 -v $(pwd):/app:rw -it ionic /bin/bash

Append the ionic command to the end of your `docker run` command. The above
example uses `ash`.


##### Volumes

Volume          | Description
----------------|-------------
`/app`          | The location of the ionic application root.

## Meta

[@A1CY0N](https://mamot.fr/@a1c0n) – a1cy0n@tutanota.com

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://gitlab.com/A1CY0N](https://gitlab.com/A1CY0N)

## Contributing

1. Fork it (<https://gitlab.com/A1CY0N/ionic_docker>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request
